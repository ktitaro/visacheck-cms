#!/bin/sh

echo "Initializing database..."

while ! nc -z "$DATABASE_HOST" "$DATABASE_PORT"; do
  sleep 0.1
done

echo "Database initialized."

exec "$@"
