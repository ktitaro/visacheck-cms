#!/bin/sh

errorlog="./error.log"

if [ -z "$1" ]; then
  echo "Enter correct docker container ID"
  exit 1
fi

if [ -z "$2" ]; then
  echo "Enter correct path to dump file"
  exit 1
fi

if cat "$2" | docker exec -i "$1" psql -U postgres 2> "$errorlog"; then
  echo "Error occured, details here: $errorlog"
else
  echo "Dump file has been imported"
fi
