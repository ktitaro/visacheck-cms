#!/bin/sh

tempfile="./temp-dump"
dumpfile="dump_$(date +%d-%m-%Y"_"%H_%M_%S).sql"

if [ -z "$1" ]; then
  echo "Enter corrent docker container ID"
  exit 1
fi

if docker exec "$1" pg_dumpall -c -U postgres &>"$tempfile"; then
  cat "$tempfile" > "$dumpfile"
else
  echo "Error occured"
fi

rm $tempfile

echo "Dump file has been exported"
