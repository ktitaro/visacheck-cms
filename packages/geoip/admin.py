from django.contrib import admin

from .models import *


@admin.register(GeoIP)
class GeoIPAdmin(admin.ModelAdmin):
    """
    Represents the UI for `GeoIP`
    """
    search_fields = ('id',)