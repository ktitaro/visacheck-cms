from ipaddress import ip_address, ip_network
from django.db import models, connection

from packages.geo.models import Country


class GeoIPManager(models.Manager):
    """
    Represents the query manager with additional functionality 
    specific for searching tcp/ip networks which contains ip address
    """
    def contain_ip(self, ip):
        ip = ip_address(ip)

        with connection.cursor() as cursor:
            cursor.execute(f"""
                SELECT
                    g.id, 
                    g.country_id,
                    g.network_ip_v4, 
                    g.network_ip_v6, 
                    g.is_anonymous_proxy, 
                    g.is_satellite_provider
                FROM geoip_geoip AS g
                WHERE cast(g.network_ip_v{ip.version} as inet) >> inet '{str(ip)}';
            """)

            results = []

            for row in cursor.fetchall():
                results.append(self.model(
                    id=row[0],
                    country_id=row[1],
                    network_ip_v4=row[2],
                    network_ip_v6=row[3],
                    is_anonymous_proxy=row[4],
                    is_satellite_provider=row[5]
                ))
                
            return results


class GeoIP(models.Model):
    """
    Represents the mapping between tcp/ip networks and the 
    countries of the world, used for user's passport suggestion
    """
    country = models.ForeignKey(Country, on_delete=models.SET_NULL, blank=True, null=True)
    network_ip_v4 = models.CharField(max_length=18, unique=True, blank=True, null=True)
    network_ip_v6 = models.CharField(max_length=48, unique=True, blank=True, null=True)
    is_anonymous_proxy = models.BooleanField(default=False)
    is_satellite_provider = models.BooleanField(default=False)
    
    objects = GeoIPManager()

    class Meta:
        verbose_name = 'Geo IP Object'
        verbose_name_plural = 'Geo IP Objects'

    def save(self, *args, **kwargs):
        if not (self.network_ip_v4 or self.network_ip_v6):
            raise ValueError('Network address must be presented!')
        
        if self.network_ip_v4:
            ip = ip_network(self.network_ip_v4)
            if ip.version != 4:
                raise ValueError('IPv4 Network has incorrect value!')

        if self.network_ip_v6:
            ip = ip_network(self.network_ip_v6)
            if ip.version != 6:
                raise ValueError('IPv6 Network has incorrect value!')

        super(GeoIP, self).save(*args, **kwargs)