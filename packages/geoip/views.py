from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from .models import *


@api_view(['GET'])
@permission_classes([AllowAny])
def identify(request):
    """
    Trying to identify `Country` by user's ip address
    """
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')

    geoip = GeoIP.objects.contain_ip(ip)
    if len(geoip) != 0:
        country = geoip[0].country
        return Response({
            "success": True,
            "country": {
                "id": country.id,
                "name_en": country.name_en,
                "name_ru": country.name_ru,
            },
        })

    return Response({ "success": False })
