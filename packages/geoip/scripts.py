import json
from csv import DictReader
from django.conf import settings

from packages.geo.models import *
from .models import GeoIP


def import_networks_datasets():
    blocks_ipv4_path = settings.BASE_DIR / 'temp/GeoLite2-Country-Blocks-IPv4.csv'
    blocks_ipv6_path = settings.BASE_DIR / 'temp/GeoLite2-Country-Blocks-IPv6.csv'
    locations_en_path = settings.BASE_DIR / 'temp/GeoLite2-Country-Locations-en.csv'

    def read_into_list(dataset_path):
        with open(dataset_path, newline='\n') as file:
            reader = DictReader(file, delimiter=',')
            return [x for x in reader]

    def read_locations_into_dict(dataset_path):
        with open(dataset_path, newline='\n') as file:
            reader = DictReader(file, delimiter=',')
            result = {}
            for row in reader:
                result[int(row['geoname_id'])] = row
            return result

    blocks_ipv4 = read_into_list(blocks_ipv4_path)
    blocks_ipv6 = read_into_list(blocks_ipv6_path)
    locations = read_locations_into_dict(locations_en_path)

    countries_cache = {}

    def create_geo_ip_object(dataset, version):
        rejected = []

        for row in dataset:
            try:
                country = None
                predicate = None

                if row['geoname_id']:
                    predicate = int(row['geoname_id'])
                else:
                    if row['registered_country_geoname_id']:
                        predicate = int(row['registered_country_geoname_id'])
                    else:
                        continue

                if countries_cache.get(predicate):
                    country = countries_cache[predicate]
                else:
                    country_obj = locations[predicate]
                    country_iso = country_obj['country_iso_code']
                    country = Country.objects.filter(iso_3166_alpha_2=country_iso).first()
                    countries_cache[predicate] = country

                geoip = GeoIP()
                geoip.country = country
                geoip.is_anonymous_proxy = bool(int(row['is_anonymous_proxy']))
                geoip.is_satellite_provider = bool(int(row['is_satellite_provider']))

                if version == 4:
                    geoip.network_ip_v4 = row['network']
                else:
                    geoip.network_ip_v6 = row['network']

                geoip.save()

            except ValueError:
                rejected.append(row)
                continue
            
            except Exception as error:
                print('Error occured while parsing geo ip object')
                print('error = ', error)
                print('row = ', row)
                _ = input('Write anything to continue ')
                rejected.append(row)

        if len(rejected) > 0:
            with open(f'./rejected-in-ipv{version}.txt', 'w+') as file:
                file.write(json.dumps(rejected))

    create_geo_ip_object(blocks_ipv4, 4)
    create_geo_ip_object(blocks_ipv6, 6)