from django.db import models

from packages.geo.models import *
from packages.iso.models import *


class Price(models.Model):
    """
    Represents a price in particular currency
    """
    amount = models.PositiveIntegerField()
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Price'
        verbose_name_plural = 'Prices'

    def __str__(self):
        return f'{self.amount} {currency.alpha}'


class Visa(models.Model):
    """
    Represents a particular visa -
    the document allowing you to enter a country.
    """
    ONLINE = 1
    IN_AMBASSY = 2
    ON_ARRIVAL = 3

    APPLICATION_TYPES = (
        (ONLINE, 'Online'),
        (IN_AMBASSY, 'In ambassy'),
        (ON_ARRIVAL, 'On arrival'),
    )

    country_from = models.ForeignKey(Country, on_delete=models.CASCADE, blank=True, null=True)
    country_to = models.ForeignKey(Country, on_delete=models.CASCADE, blank=True, null=True)

    application_type = models.SmallPositiveIntegerField(choices=APPLICATION_TYPES, blank=True, null=True)
    application_price = models.ForeignKey(Price, on_delete=models.SET_NULL, blank=True, null=True)

    #TODO:
    # It needs to be a different entity
    # where a document have an example,
    # description, maybe a form.
    required_documents = models.TextField()


    class Meta:
        verbose_name = 'Visa'
        verbose_name_plural = 'Visas'

    def __str__(self):
        return f'{self.country_from.name_en} -> {self.country_to.name_en}'