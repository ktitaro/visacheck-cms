from rest_framework import serializers

from packages.files.serializers import *
from .models import *


class RegionSerializer(serializers.ModelSerializer):
    """
    Represents serializer for the `Region`
    """
    class Meta:
        model = Region
        fields = (
            'id', 
            'url', 
            'name_en',
            'name_ru',
            'slug',
        )


class SubregionSerializer(serializers.ModelSerializer):
    """
    Represents serializer for the `Subregion`
    """
    class Meta:
        model = Subregion
        fields = (
            'id', 
            'url', 
            'name_en',
            'name_ru',
            'slug',
        )


class CountrySerializer(serializers.ModelSerializer):
    """
    Represents serializer for the `Country`
    """
    class Meta:
        model = Country
        fields = (
            'id', 
            'url', 
            'name_en',
            'name_ru',
            'slug',
        )
