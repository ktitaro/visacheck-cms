from django.db import models

from packages.iso.models import ISO_3166_1
from packages.files.models import Picture, Gallery


class BaseGeoEntity(models.Model):
    """
    Represents the set of attributes essential for 
    describing the following geographical entities
    """
    name_en = models.CharField(max_length=64, blank=True, null=True)
    name_ru = models.CharField(max_length=64, blank=True, null=True)
    slug = models.SlugField(blank=True, null=True)

    gallery = models.ForeignKey(Gallery, on_delete=models.SET_NULL, blank=True, null=True, related_name='+')
    preview = models.ForeignKey(Picture, on_delete=models.SET_NULL, blank=True, null=True, related_name='+')
    thumbnail = models.ForeignKey(Picture, on_delete=models.SET_NULL, blank=True, null=True, related_name='+')
    background = models.ForeignKey(Picture, on_delete=models.SET_NULL, blank=True, null=True, related_name='+')

    class Meta:
        abstract = True
        ordering = ('-name_en',)

    def __str__(self):
        return self.name_en or self.name_ru

    def save(self, *args, **kwargs):
        if self.slug is None:
            if self.name_en:
                parts = self.name_en.split(' ')
                parts = map(lambda x: x.lower(), parts)
                self.slug = '-'.join(parts)
        super(BaseGeoEntity, self).save(*args, **kwargs)


class Region(BaseGeoEntity):
    """
    Represents the region of the world
    """
    pass

    class Meta:
        verbose_name = 'Region'
        verbose_name_plural = 'Regions'


class Subregion(BaseGeoEntity):
    """
    Represents the subregion of the world
    """
    region = models.ForeignKey(Region, on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        verbose_name = 'Subregion'
        verbose_name_plural = 'Subregions'


class Country(BaseGeoEntity):
    """
    Represents the country of the world
    """
    parent = models.ForeignKey('self', on_delete=models.SET_NULL, blank=True, null=True)
    iso_3166 = models.ForeignKey(ISO_3166_1, on_delete=models.SET_NULL, blank=True, null=True)
    subregions = models.ManyToManyField(Subregion, blank=True)
    is_unrecognized = models.BooleanField(blank=True, default=False)

    class Meta:
        verbose_name = 'Country'
        verbose_name_plural = 'Countries'

    @property
    def is_subdivision(self):
        return self.parent != None


class Territory(BaseGeoEntity):
    """
    Represents a particular territory within `Country`
    """
    country = models.ForeignKey(Country, on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        verbose_name = 'Territory'
        verbose_name_plural = 'Territories'