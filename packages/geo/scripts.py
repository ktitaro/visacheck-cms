import json
from csv import DictReader
from django.conf import settings

from packages.iso.models import *
from .models import *


def import_regions_dataset():
    dataset_path = settings.BASE_DIR / 'temp/places_region.csv'

    with open(dataset_path, newline='\n') as file:
        reader = DictReader(file, delimiter=',')

        for row in reader:
            item = Region(name_ru=row['name'], slug=row['slug'])
            item.save()


def import_subregions_dataset():
    dataset_path = settings.BASE_DIR / 'temp/places_subregion.csv'

    with open(dataset_path, newline='\n') as file:
        reader = DictReader(file, delimiter=',')

        for row in reader:
            item = Subregion(name_ru=row['name'], slug=row['slug'])
            item.save()


def import_countries_dataset():
    dataset_path = settings.BASE_DIR / 'temp/places_country.csv'

    with open(dataset_path, newline='\n') as file:
        reader = DictReader(file, delimiter=',')

        for row in reader:
            item = Country(name_ru=row['name'], slug=row['slug'])
            item.save()


def import_relations():
    regions_dataset_path = settings.BASE_DIR / 'temp/places_region.csv'
    subregions_dataset_path = settings.BASE_DIR / 'temp/places_subregion.csv'
    countries_dataset_path = settings.BASE_DIR / 'temp/places_country.csv'
    country_regions_dataset_path = settings.BASE_DIR / 'temp/places_country_regions.csv'
    country_subregions_dataset_path = settings.BASE_DIR / 'temp/places_country_subregions.csv'

    def read_into_list(dataset_path):
        with open(dataset_path, newline='\n') as file:
            reader = DictReader(file, delimiter=',')
            return [row for row in reader]

    regions = read_into_list(regions_dataset_path)
    subregions = read_into_list(subregions_dataset_path)
    countries = read_into_list(countries_dataset_path)
    country_regions = read_into_list(country_regions_dataset_path)
    country_subregions = read_into_list(country_subregions_dataset_path)

    regions_cache = {}
    subregions_cache = {}
    countries_cache = {}

    for item in country_regions:
        region_entry = None
        country_entry = None

        if regions_cache.get(int(item['region_id'])):
            region_entry = regions_cache.get(int(item['region_id']))
        else:
            region_obj = list(filter(lambda x: int(x['id']) == int(item['region_id']), regions))[0]
            region_entry = Region.objects.filter(slug=region_obj['slug']).first()
            regions_cache[int(item['region_id'])] = region_entry

        if countries_cache.get(int(item['country_id'])):
            country_entry = countries_cache.get(int(item['country_id']))
        else:
            country_obj = list(filter(lambda x: int(x['id']) == int(item['country_id']), countries))[0]
            country_entry = Country.objects.filter(slug=country_obj['slug']).first()
            countries_cache[int(item['country_id'])] = country_entry

        country_entry.region = region_entry
        country_entry.save()

    for item in country_subregions:
        subregion_entry = None
        country_entry = None

        if subregions_cache.get(int(item['subregion_id'])):
            subregion_entry = subregions_cache.get(int(item['subregion_id']))
        else:
            subregion_obj = list(filter(lambda x: int(x['id']) == int(item['subregion_id']), subregions))[0]
            subregion_entry = Subregion.objects.filter(slug=subregion_obj['slug']).first()
            subregions_cache[int(item['subregion_id'])] = subregion_entry

        if countries_cache.get(int(item['country_id'])):
            country_entry = countries_cache.get(int(item['country_id']))
        else:
            country_obj = list(filter(lambda x: int(x['id']) == int(item['country_id']), countries))[0]
            country_entry = Country.objects.filter(slug=country_obj['slug']).first()
            countries_cache[int(item['country_id'])] = country_entry

        country_entry.subregion = subregion_entry
        country_entry.save()


def derive_en_name_from_slug():
    countries = Country.objects.all()
    ignore = ['and', 'the', 'a', 'of']

    for country in countries:
        parts = country.slug.split('-')
        buffer = []

        for part in parts:
            if part in ignore:
                buffer.append(part)
            else:
                buffer.append(f'{part[0:1].upper()}{part[1:]}')

        country.name_en = ' '.join(buffer)
        country.save()


def import_iso_3166():
    dataset_path = settings.BASE_DIR / 'temp/iso_3166.json'

    with open(dataset_path) as file:
        reader = json.loads(file.read())
        countries = list(Country.objects.all())
        countries_names = list(map(lambda x: x.name_en.lower(), countries))
        unrecognized = []

        for item in reader:
            name = item['name'].lower()

            if name in countries_names:
                country = countries[countries_names.index(name)]
                country.iso_3166_alpha_2 = item['alpha2']
                country.iso_3166_alpha_3 = item['alpha3']
                country.iso_3166_numeric = item['numeric']
                country.save()
            else:
                unrecognized.append(item['name'])

        if len(item['name']) != 0:
            print(f'There are {len(unrecognized)} unrecognized countries')
            print(unrecognized)


def import_data():
    import_regions_dataset()
    import_subregions_dataset()
    import_countries_dataset()
    import_relations()
    derive_en_name_from_slug()


def link_existed_iso_3166():
    countries = Country.objects.all()

    for country in countries:
        if all([
            country.iso_3166_alpha_2,
            country.iso_3166_alpha_3,
            country.iso_3166_numeric
        ]):
            iso_3166 = ISO_3166_1.objects.get(alpha_2=country.iso_3166_alpha_2)
            country.iso_3166 = iso_3166
            country.save()