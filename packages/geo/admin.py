from django.contrib import admin

from .models import *


@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    list_display = ('name_en', 'name_ru',)
    fieldsets = (
        (None, {
            'fields': (
                'name_en', 
                'name_ru',
                'slug',
            )
        }),
        ('Media Files', {
            'fields': (
                'preview', 
                'thumbnail', 
                'background', 
                'gallery',
            )
        }),
    )


@admin.register(Subregion)
class SubregionAdmin(admin.ModelAdmin):
    list_display = ('name_en', 'name_ru',)
    fieldsets = (
        (None, {
            'fields': (
                'name_en',
                'name_ru',
                'slug', 
                'region',
            )
        }),
        ('Media Files', {
            'fields': (
                'preview', 
                'thumbnail', 
                'background', 
                'gallery',
            )
        }),
    )


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ('name_en', 'name_ru',)
    search_fields = ('name_en', 'name_ru',)
    fieldsets = (
        (None, {
            'fields': (
                'name_en',
                'name_ru',
                'slug',
                'subregions',
                'parent',
                'iso_3166',
                'is_unrecognized',
            ),
        }),
        ('Media Files', {
            'fields': (
                'preview', 
                'thumbnail', 
                'background', 
                'gallery',
            ),
        }),
    )


@admin.register(Territory)
class TerritoryAdmin(admin.ModelAdmin):
    list_display = ('name_en', 'name_ru',)
    search_fields = ('name_en', 'name_ru',)
    fieldsets = (
        (None, {
            'fields': (
                'name_en',
                'name_ru',
                'slug',
                'country',
            ),
        }),
        ('Media Files', {
            'fields': (
                'preview', 
                'thumbnail', 
                'background', 
                'gallery',
            ),
        }),
    )