from rest_framework import viewsets

from .models import *
from .serializers import *


class RegionViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Represents ViewSet for the `Region`
    """
    queryset = Region.objects.all()
    serializer_class = RegionSerializer

    def get_serializer_context(self):
        context = super(RegionViewSet, self).get_serializer_context()
        context.update({'request': self.request})
        return context


class SubregionViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Represents ViewSet for the `Subregion`
    """
    queryset = Subregion.objects.all()
    serializer_class = SubregionSerializer

    def get_serializer_context(self):
        context = super(SubregionViewSet, self).get_serializer_context()
        context.update({'request': self.request})
        return context


class CountryViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Represents ViewSet for the `Country`
    """
    queryset = Country.objects.all()
    serializer_class = CountrySerializer

    def get_serializer_context(self):
        context = super(CountryViewSet, self).get_serializer_context()
        context.update({'request': self.request})
        return context