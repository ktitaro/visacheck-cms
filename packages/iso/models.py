from django.db import models


class ISO_3166_1(models.Model):
    """
    Represents ISO 3166-1 standard -
    codes for the representation of names of countries and their subdivisions
    """
    alpha_2 = models.CharField(max_length=2, unique=True)
    alpha_3 = models.CharField(max_length=3, unique=True)
    numeric = models.CharField(max_length=3, unique=True)

    class Meta:
        verbose_name = 'ISO 3166-1 - Countries'
        verbose_name_plural = 'ISO 3166-1 - Countries'

    def __str__(self):
        return self.alpha_2


class ISO_4217(models.Model):
    """
    Represents ISO 4217 standard -
    codes for the representation of currencies and theis minor units
    """
    name_en = models.CharField(max_length=64, unique=True, blank=True, null=True)
    name_ru = models.CharField(max_length=64, unique=True, blank=True, null=True)
    alpha = models.CharField(max_length=3, unique=True)
    numeric = models.CharField(max_length=3, unique=True)

    class Meta:
        verbose_name = 'ISO 4217 - Currencies'
        verbose_name_plural = 'ISO 4217 - Currencies'

    def __str__(self):
        return self.alpha


class Currency(ISO_4217):
    """
    Represents an alias for `ISO_4217`
    """
    class Meta:
        proxy = True


class ISO_639(models.Model):
    """
    Represents ISO 639-* standards -
    codes for representation of languages
    """
    name_en = models.CharField(max_length=100, unique=True, blank=True, null=True)
    name_ru = models.CharField(max_length=100, unique=True, blank=True, null=True)
    iso_639_1 = models.CharField(max_length=2, unique=True)
    iso_639_2_t = models.CharField(max_length=3, unique=True)
    iso_639_2_b = models.CharField(max_length=3, unique=True)

    class Meta:
        verbose_name = 'ISO 639 - Languages'
        verbose_name_plural = 'ISO 639 - Languages'

    def __str__(self):
        return self.iso_639_1


class Language(ISO_639):
    """
    Represents an alias for `ISO_639`
    """
    class Meta:
        proxy = True