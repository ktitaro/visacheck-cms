from django.contrib import admin

from .models import *


@admin.register(ISO_3166_1)
class ISO_3166_1Admin(admin.ModelAdmin):
    """
    Represents the UI for `ISO_3166_1`
    """
    list_display = ('alpha_2', 'alpha_3', 'numeric')
    search_fields = ('alpha_2', 'alpha_3', 'numeric')


@admin.register(ISO_4217)
class ISO_4217Admin(admin.ModelAdmin):
    """
    Represents the UI for `ISO_4217`
    """
    list_display = ('name_en', 'alpha', 'numeric')
    search_fields = ('name_en', 'alpha', 'numeric')


@admin.register(ISO_639)
class ISO_639Admin(admin.ModelAdmin):
    """
    Represents the UI for `ISO_639`
    """
    list_display = ('name_en', 'iso_639_1', 'iso_639_2_t', 'iso_639_2_b')
    search_fields = ('name_en', 'iso_639_1', 'iso_639_2_t', 'iso_639_2_b')
