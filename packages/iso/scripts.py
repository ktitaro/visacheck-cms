import json
from django.conf import settings

from packages.geo.models import *
from .models import *


def import_existed_iso_3166():
    countries = Country.objects.all()

    for country in countries:
        if all([
            country.iso_3166_alpha_2,
            country.iso_3166_alpha_3,
            country.iso_3166_numeric
        ]):
            item = ISO_3166_1(
                alpha_2=country.iso_3166_alpha_2,
                alpha_3=country.iso_3166_alpha_3,
                numeric=country.iso_3166_numeric,
            )
            item.save()


def import_iso_4217_dataset():
    dataset_path = settings.BASE_DIR / 'temp/iso_4217.json'

    with open(dataset_path) as file:
        reader = json.loads(file.read())

        for item in reader:
            try:
                iso = ISO_4217(
                    alpha=item['alpha_3'],
                    numeric=item['numeric'],
                    name_en=item['name_en'],
                )
                iso.save()
            except Exception as error:
                if str(error).find('duplicate key value') != -1:
                    continue
                else:
                    print(error)
                    print(item)
                    break


def import_iso_639():
    dataset_path = settings.BASE_DIR / 'temp/iso_639.json'

    with open(dataset_path) as file:
        reader = json.loads(file.read())

        for item in reader:
            try:
                iso = ISO_639(
                    name_en=item['name'],
                    iso_639_1=item['alpha_639_1'],
                    iso_639_2_t=item['alpha_639_2_t'],
                    iso_639_2_b=item['alpha_639_2_b'],
                )
                iso.save()
            except Exception as error:
                if str(error).find('duplicate key value') != -1:
                    continue
                else:
                    print(error)
                    print(item)
                    break