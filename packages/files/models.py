from django.db import models

from .helpers import UUID4FileName


class Picture(models.Model):
    """
    Represents the picture file
    """
    src = models.ImageField(upload_to=UUID4FileName(prefix='pictures'))

    class Meta:
        verbose_name = 'Picture'
        verbose_name_plural = 'Pictures'


class OrderedPicture(models.Model):
    """
    Represents the through model for ordering multiple pictures
    """
    order = models.PositiveSmallIntegerField(default=0)
    picture = models.ForeignKey(Picture, on_delete=models.CASCADE)
    gallery = models.ForeignKey('Gallery', on_delete=models.CASCADE)


class Gallery(models.Model):
    """
    Represents the gallery of pictures
    """
    pictures = models.ManyToManyField(Picture, through='OrderedPicture')

    class Meta:
        verbose_name = 'Gallery'
        verbose_name_plural = 'Galleries'