from rest_framework import serializers

from .models import *


class PictureSerializer(serializers.ModelSerializer):
    """
    Represents serializer for the `Picture`
    """
    src = serializers.SerializerMethodField()

    class Meta:
        model = Picture
        fields = ('src',)

    def get_src(self, obj):
        request = self.context.get('request')
        if request:
            return request.build_absolute_uri(obj.src.url)
        return obj.src.url


class OrderedPictureSerializer(serializers.ModelSerializer):
    """
    Represents serializer for the `OrderedPicture`
    """
    picture = serializers.SerializerMethodField()

    class Meta:
        model = OrderedPicture
        fields = ('picture', 'order')

    def get_picture(self, obj):
        request = self.context.get('request')
        if request:
            return request.build_absolute_uri(obj.picture.src.url)
        return obj.picture.src.url


class GallerySerializer(serializers.ModelSerializer):
    """
    Represents serializer for the `Gallery`
    """
    pictures = serializers.SerializerMethodField()

    class Meta:
        model = Gallery
        fields = ('pictures',)

    def get_pictures(self, obj):
        request = self.context.get('request')
        context = {"request": request}
        queryset = obj.orderedimage_set.all()
        return [OrderedImageSerializer(i, context=context).data for i in queryset]
