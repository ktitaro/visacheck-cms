from uuid import uuid4
from os.path import join, splitext

from django.utils.deconstruct import deconstructible


@deconstructible
class UUID4FileName(object):
  """
  Defines UUIDv4 names for uploading files.
  """
  def __init__(self, prefix=''):
    self.pattern = join(prefix, '%s%s')

  def __call__(self, _, filename):
    extension = splitext(filename)[1]
    return self.pattern % (uuid4(), extension)
