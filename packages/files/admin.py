from django.contrib import admin

from .models import *


@admin.register(Picture)
class PictureAdmin(admin.ModelAdmin):
    """
    Represents the UI for `Picture`
    """
    pass


class OrderedPictureInline(admin.TabularInline):
    """
    Represents the UI for `OrderedPicture`
    """
    model = OrderedPicture
    extra = 1


@admin.register(Gallery)
class GalleryAdmin(admin.ModelAdmin):
    """
    Represents the UI for `Gallery`
    """
    inlines = (OrderedPictureInline,)