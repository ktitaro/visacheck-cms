from django.urls import path, include
from rest_framework import routers

from packages.geo.views import *


router = routers.DefaultRouter()

router.register(r'geo/regions', RegionViewSet)
router.register(r'geo/countries', CountryViewSet)
router.register(r'geo/subregions', SubregionViewSet)

urlpatterns = [
    path('geoip/', include('packages.geoip.urls')),
]

urlpatterns += router.urls